FROM node:alpine as builder

WORKDIR /app
ADD package.json /app/package.json
RUN npm config set registry http://registry.npmjs.org
RUN npm install;
COPY . /app
RUN npm run build


FROM node:alpine

WORKDIR /app
COPY --from=builder /app ./
ARG NODE_ENV=prod
ENV NODE_ENV=${NODE_ENV}
CMD ["npm", "run", "start:prod"]
