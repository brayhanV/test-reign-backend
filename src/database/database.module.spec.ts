import { Test, TestingModule } from '@nestjs/testing';
import { DatabaseModule } from './database.module';

describe('DatabaseModule', () => {
  let appModule: DatabaseModule;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      imports: [DatabaseModule],
    }).compile();

    appModule = app.get<DatabaseModule>(DatabaseModule);
  });

  describe('root', () => {
    it('initialize"', () => {
      expect(appModule).toBeTruthy();
    });
  });
});
