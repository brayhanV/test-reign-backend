import { enviroments } from './enviroments';

describe('Enviroments', () => {
  it('should return { dev: ".env", prod: ".prod.env"}', () => {
    expect(enviroments.dev).toBe('.env');
    expect(enviroments.prod).toBe('.prod.env');
  });
});
