import { HttpModule } from '@nestjs/axios';
import { getModelToken } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import { Model } from 'mongoose';
import { Articles, ArticlesDocument } from '../entities/article.entity';
import { ArticlesService } from './articles.service';

describe('ArticlesService', () => {
  let service: ArticlesService;
  let mockArticleModel: Model<ArticlesDocument>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [HttpModule],
      providers: [
        ArticlesService,
        {
          provide: getModelToken(Articles.name),
          useValue: Model,
        },
      ],
    }).compile();

    service = module.get<ArticlesService>(ArticlesService);
    mockArticleModel = module.get<Model<ArticlesDocument>>(
      getModelToken(Articles.name),
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should return a article doc', async () => {
    const article = new Articles();
    const objectID = 1;
    article.objectID = objectID;

    const spy = jest
      .spyOn(mockArticleModel, 'findOne')
      .mockResolvedValue(article as ArticlesDocument);

    await mockArticleModel.findOne({ objectID });

    expect(spy).toHaveBeenCalled();
  });
});
