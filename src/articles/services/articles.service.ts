import { HttpService } from '@nestjs/axios';
import { Injectable, Logger, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Cron, CronExpression } from '@nestjs/schedule';
import { Model } from 'mongoose';
import { lastValueFrom } from 'rxjs';
import { Articles } from '../entities/article.entity';

@Injectable()
export class ArticlesService {
  constructor(
    @InjectModel(Articles.name) private articleModel: Model<Articles>,
    private http: HttpService,
  ) {}

  private readonly logger = new Logger(ArticlesService.name);

  @Cron(CronExpression.EVERY_HOUR)
  async updateArticles() {
    const hitsPerPage = 1000;
    let maxHits = 0;
    let hitsCount = 0;

    do {
      const {
        data: { hits, nbHits },
      } = await lastValueFrom(
        this.http.get(
          `https://hn.algolia.com/api/v1/search_by_date?query=nodejs&page=0&hitsPerPage=${hitsPerPage}`,
        ),
      );

      maxHits = nbHits;

      hits.forEach(async (elem) => {
        const article = await this.findById(elem.objectID);

        if (!article) {
          const newArticle = new this.articleModel({
            objectID: elem.objectID,
            storyTitle: elem['story_title'],
            title: elem.title,
            author: elem.author,
            createdAt: elem['created_at'],
            url: elem.url,
            storyUrl: elem['story_url'],
          });

          await newArticle.save();
        }
      });

      hitsCount += hits.length;
    } while (hitsCount < maxHits);
  }

  async findAll() {
    const all = await this.articleModel
      .find({ deleted: false })
      .sort({ createdAt: -1 })
      .exec();

    return all;
  }

  findById(objectID: number) {
    return this.articleModel.findOne({ objectID }).exec();
  }

  findDeleted(onlyIds = false) {
    const query = this.articleModel.find({ deleted: true });

    if (onlyIds) {
      query.select('objectID');
    }

    return query.exec();
  }

  purge() {
    return this.articleModel.deleteMany();
  }

  delete(objectID: number) {
    const article = this.articleModel
      .findOneAndUpdate({ objectID }, { deleted: true })
      .exec();

    if (!article) {
      throw new NotFoundException(
        `Article with objectId ${objectID} doesn't exist`,
      );
    }

    return article;
  }
}
