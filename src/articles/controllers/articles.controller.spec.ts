import { HttpModule } from '@nestjs/axios';
import { getModelToken } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import { Model } from 'mongoose';
import { ArticlesModule } from '../articles.module';
import { Articles } from '../entities/article.entity';
import { ArticlesController } from './articles.controller';

describe('ArticlesController', () => {
  let controller: ArticlesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [ArticlesModule, HttpModule],
      controllers: [ArticlesController],
      providers: [
        {
          provide: getModelToken(Articles.name),
          useValue: Model,
        },
      ],
    }).compile();

    controller = module.get<ArticlesController>(ArticlesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
