import {
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { ArticlesService } from '../services/articles.service';

@ApiTags('articles')
@Controller('articles')
export class ArticlesController {
  constructor(private readonly articleService: ArticlesService) {}

  @Get()
  getArticles() {
    return this.articleService.findAll();
  }

  @Get('deleted')
  getArticlesDeleted() {
    return this.articleService.findDeleted();
  }

  @Get(':id')
  getArticleByObjectId(@Param('id', ParseIntPipe) id: number) {
    return this.articleService.findById(id);
  }

  @Post('pullArticles')
  bulkArticles() {
    return this.articleService.updateArticles();
  }

  @Delete('purge')
  purgeArticles() {
    return this.articleService.purge();
  }

  @Delete(':id')
  deleteArticle(@Param('id', ParseIntPipe) id: number) {
    return this.articleService.delete(id);
  }
}
