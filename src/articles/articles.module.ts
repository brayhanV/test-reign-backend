import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ScheduleModule } from '@nestjs/schedule';
import { ArticlesController } from './controllers/articles.controller';
import { Articles, ArticleSchema } from './entities/article.entity';
import { ArticlesService } from './services/articles.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: Articles.name,
        schema: ArticleSchema,
      },
    ]),
    ScheduleModule.forRoot(),
    HttpModule,
  ],
  controllers: [ArticlesController],
  providers: [ArticlesService],
  exports: [ArticlesService],
})
export class ArticlesModule {}
