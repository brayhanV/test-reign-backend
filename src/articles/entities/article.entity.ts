import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type ArticlesDocument = Articles & Document;

@Schema()
export class Articles {
  @Prop({ required: true, index: true, unique: true })
  objectID: number;

  @Prop()
  storyTitle: string;

  @Prop()
  title: string;

  @Prop({ required: true })
  author: string;

  @Prop({ type: Date, required: true })
  createdAt: Date;

  @Prop()
  url: string;

  @Prop()
  storyUrl: string;

  @Prop({ required: true, default: false })
  deleted: boolean;
}

export const ArticleSchema = SchemaFactory.createForClass(Articles);
